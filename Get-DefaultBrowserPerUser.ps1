Function Get-DefaultBrowserPerUser {
    <#
        .SYNOPSIS
        Pull ProgId value from HKEY_USERS\$Sid\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice

        .DESCRIPTION
        Traverses each SID stored in HKEY_USERS and detects the default browser that the user has configured.
        Useful for checking adherence to policy surrounding browser use in your environment
        
        .PARAMETER Computers
        An array of computers you wish to query for default browser. 
        This parameter is REQUIRED

        .PARAMETER LogPath
        The file you wish to log data too. Does not support CSV at this time.

        .EXAMPLE

        Get-DefaultBrowserPerUser -Computers mypc,yourpc,lonelypc -LogPath C:\Logs\myawesomelog.txt

        .EXAMPLE 
        
        Get-DefaultBrowserPerUser -Computers (Get-Content -Path C:\Files\computers.txt) -LogPath C:\Logs\myawesomelog.txt

        .EXAMPLE

        Get-DefaultBrowserPerUSer -Computers (Get-ADComputer -Filter * -SearchBase "OU=Computers,DC=domain,DC=local" | Select -Expand Name) -LogPath C:\Logs\mylog.txt

        .EXAMPLE

        $params = @{
            'Computers' = @('pc1','pc2','pc3')
            'LogPath' = C:\Logs\myawesomelog.txt
        }

        Get-DefaultBrowserPerUser @params
    #>

    Param(
        [cmdletBinding()]
        [Parameter(Mandatory, Position = 0)]
        [ValidateNotNullOrEmpty()]
        [String[]]$Computers,
        [Parameter(Mandatory, Position = 1)]
        [String]$LogPath
    )

    Foreach ($computer in $Computers) {
        #Attempt testing PSRemoting, log failures.
        Try {
            
            Test-WSMan -ComputerName $computer | Out-Null
            Write-Output "Data captured from: $computer`n" | Out-File $LogPath -Append
        
        }

        Catch {
                
            Write-Output "Powershell Remoting is not working on: $computer `n`n" | Out-File $LogPath -Append
            
        }

        #Capture the remote output into a variable.
        $remoteDeserializedData = Try {
            Invoke-Command -ComputerName $computer -ScriptBlock {

                If (!(Test-Path HKU:\)) {
    
                    New-PSDrive -Name HKU -PSProvider Registry -Root HKEY_USERS | Out-Null

                }

                $Sids = @(Get-WmiObject win32_userprofile |
                        Where-Object {$($_.SID).Length -gt 8} |
                        Select-Object SID, @{N = 'LocalPath'; E = {$($_.LocalPath).SubString(9)}})

                Foreach ($Sid in $Sids) {
                    
                    If (!(Test-Path HKU:\$($Sid.SID)\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice)) {
        
                        Write-Output "$($Sid.LocalPath) has no profile to check!"
                    }

                    Else {
                        
                        $Browser = Get-ItemProperty HKU:\$($Sid.SID)\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice |
                            Select-Object -ExpandProperty ProgId
                        Switch ($Browser) {
                            'ChromeHTML' { Write-Output "$($Sid.LocalPath)'s Default Browser is: Google Chrome" }
                            'IE.HTTP' { Write-Output "$($Sid.LocalPath)'s Default Browswer is: Internet Explorer" }
                            'FirefoxURL-308046B0AF4A39CB' { Write-Output "$($Sid.LocalPath)'s Default Browser is: Mozilla FireFox" }
                            'AppXq0fevzme2pys62n3e0fbqa7peapykr8v' { Write-Output "$($Sid.LocalPath)'s Default Browser is: Microsoft Edge" }
                        }
                    }

                }

            } -ErrorAction Stop #Invoke-Command

        }  

        Catch {
            
            Write-Warning "Could not query: $computer `n"
        
        }#catch

        $remoteDeserializedData | Out-File $LogPath -Append

    }#foreach

}#function