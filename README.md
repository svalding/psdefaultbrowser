# PSDefaultBrowswer

Scans HKEY_USERS registry hive for default web browser configuration. Useful to ensure adherence to policy.

.SYNOPSIS
        Pull ProgId value from HKEY_USERS\$Sid\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice

.DESCRIPTION
        Traverses each SID stored in HKEY_USERS and detects the default browser that the user has configured.
        Useful for checking adherence to policy surrounding browser use in your environment
        
.PARAMETER Computers
        An array of computers you wish to query for default browser. 
        This parameter is REQUIRED

.PARAMETER LogPath
        The file you wish to log data too. Does not support CSV at this time.

.EXAMPLE

        Get-DefaultBrowserPerUser -Computers mypc,yourpc,lonelypc -LogPath C:\Logs\myawesomelog.txt

.EXAMPLE 
        
        Get-DefaultBrowserPerUser -Computers (Get-Content -Path C:\Files\computers.txt) -LogPath C:\Logs\myawesomelog.txt

.EXAMPLE

        Get-DefaultBrowserPerUSer -Computers (Get-ADComputer -Filter * -SearchBase "OU=Computers,DC=domain,DC=local" | Select -Expand Name) -LogPath C:\Logs\mylog.txt

.EXAMPLE

        $params = @{
            'Computers' = @('pc1','pc2','pc3')
            'LogPath' = C:\Logs\myawesomelog.txt
        }

        Get-DefaultBrowserPerUser @params